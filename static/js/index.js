$(function(){
	console.log("Index ready!");
	set_globals();
	get_request_data();

});

let set_globals = function(){
	window.FETCH_DATA_URL = "/get_data";
	const APPROVE_REQUEST_URL = "";
	const REJECT_REQUEST_URL = "";
	window.row_template = "<tr>"+
	"<td class='sno'></td>"+
	"<td class='req_id'></td>"+
	"<td class='nameOfClaim'></td>"+
	"<td class='docs'><button class='view_docs btn btn-sm btn-outline-primary' onclick='view_docs(this)'>View</button></td>"+
	"<td class='status'></td>"+
	"<td><button class='approve btn btn-sm btn-outline-success' onclick='update_request(this, \"Approve\")'><img src=\"https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/png/check.png\" /></button></td>"+
	"<td><button class='reject btn btn-sm btn-outline-danger' onclick='update_request(this, \"Reject\")'><img src=\"https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/png/circle-x.png\" /></button></td>"+
	"</tr>";
	window.link_holder = {};
}

let get_request_data = function(){
	fetch(window.FETCH_DATA_URL, {
		method:"GET"
	}).then(function(response){
		return response.json();
	}).then(function(result){
		console.log(result);
		set_request_data(result);
	});
}

let set_request_data = function(json_data){
	
	let data_keys = Object.keys(json_data);
	console.log(data_keys);
	for(var d = 0;d<data_keys.length;d++){
		data_array = json_data[data_keys[d]];
		console.log(data_array)
		for(var r = 0; r<data_array.length;r++){
			temp_holder = $(window.row_template);
			temp_holder.find('.sno').append(data_keys[d]);
			console.log(data_array[r]['claimNum']);
			temp_holder.find('.req_id').append(data_array[r]['claimNum']);
			temp_holder.find('.view_docs').attr('data-id', data_array[r]['claimNum']);
			temp_holder.find('.approve').attr('data-id', data_keys[d]+"-"+d);
			temp_holder.find('.reject').attr('data-id', data_keys[d]+"-"+d);
			temp_holder.find('.nameOfClaim').append(data_array[r]['nameOfClaim']);
			temp_holder.find('.status').append(data_array[r]['status']);
			if(data_array[r]['status'].toLowerCase() !== 'pending'){
				temp_holder.find(".approve").prop('disabled', true);
				temp_holder.find(".reject").prop('disabled', true);
			}
			window.link_holder[data_array[r]['claimNum']] = data_array[r]['docUrls']; 
			$("tbody").append(temp_holder);	

		}
		
	}
}

let update_request = function(context, status ){
	d = $(context).attr('data-id');
	split_d = d.split("-");

	to_send = {
		'array_key' : split_d[1],
		'a_no':split_d[0],
		'status':status
	};
	
	fetch('/update', {
		"method":"POST",
		credentials: "same-origin",
	    headers: {
	        "X-CSRFToken": getCookie("csrftoken"),
	        "Accept": "application/json",
	        "Content-Type": "application/json"
	    },
		"body":JSON.stringify(to_send)
	}).then(function(response){
		return response.json();
	}).then(function(result){
		alert("Success!");
		location.reload();
	});
}



let view_docs = function(context){
	for_id = $(context).attr('data-id');
	console.log(for_id);
	link_listing = window.link_holder[for_id];
	for(var l = 0; l<link_listing.length; l++){
		var win = window.open(link_listing[l], '_blank');
  		win.focus();
	}
}

let getCookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}