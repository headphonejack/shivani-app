# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, HttpResponseRedirect
print "importing firebase"
from firebase import firebase
print firebase
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt


def home(request):
	return render(request, 'index.html', context = {})

def get_firebase_database_entries(request):
	result = None
	# try:
	fb = firebase.FirebaseApplication("https://reimburse-2ad89.firebaseio.com", None)
	result = fb.get("/claims", None)
	# except Exception as E:
	# 	print E

	return JsonResponse(result, safe=False)

@csrf_exempt
def update_firebase_entry(request):
	try:
		fb = firebase.FirebaseApplication('https://reimburse-2ad89.firebaseio.com', None)
		json_data = json.loads(request.body)
		c_id = json_data['array_key']
		aadhaar_num = json_data['a_no']
		option_type = json_data['status']
		update_string_url = '/claims/'+str(aadhaar_num)+'/'+c_id
		print update_string_url
		response_result = fb.put(update_string_url, "status",option_type)
		result = dict()
		result['message'] = response_result
		result['status'] = 200
		result['result'] = 'SUCCESS'
	except Exception as E:
		result['message'] = str(E)
		result['status'] = 500
		result['result'] = 'FAILURE'


	return JsonResponse(result)


