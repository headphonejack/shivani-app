from django.conf.urls import url
from . import views
urlpatterns = [
	url(r'^$', views.home),
	url(r'^get_data$', views.get_firebase_database_entries),
	url(r'^update$', views.update_firebase_entry),

]
